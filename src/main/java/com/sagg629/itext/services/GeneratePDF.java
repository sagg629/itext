package com.sagg629.itext.services;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Sergio A Guzman <sagg629@gmail.com> on 7/10/16 10:33 AM
 */
@RestController
@RequestMapping("services/pdf")
public class GeneratePDF {
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getFile() throws IOException, DocumentException {
        String destinationFileName = "helloWorld.pdf";
        ByteArrayOutputStream virtualFileDestination = new ByteArrayOutputStream();

        //document definition
        //page size (http://developers.itextpdf.com/examples/itext5-building-blocks/page-size)
        //page orientation (http://developers.itextpdf.com/examples/page-events-itext5/page-orientation-and-rotation)
        Document document = new Document(PageSize.A4, 36, 36, 36, 72);
        PdfWriter pdfWriter = PdfWriter.getInstance(document, virtualFileDestination);
        pdfWriter.setPageEvent(new HeaderFooter("Nini Farm", "2017-06-01 - 2017-06-05"));
        document.open();

        //metadata (http://developers.itextpdf.com/examples/miscellaneous-itext5/custom-metadata-entry)
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        document.addTitle("Documento PDF");
        document.addAuthor("Scarab Solutions");
        document.addCreator("Scarab Solutions");
        document.addSubject(String.format("PDF report of %s, retrieved at %s", "finca", dateFormat.format(new Date())));
        document.addKeywords(String.format("pdf_report, scarab, %s", "finca"));
        document.addCreationDate();

        //creation
        java.util.List<String> listImagesURL = new ArrayList<>();
        boolean firstCase = true;
        float[] tableWidths;
        if (firstCase) {
            //2nd img should be less wider than the others
            tableWidths = new float[]{571, 407, 532};
            listImagesURL.add("https://images.scarab-solutions.com/Florecal-S.A./Florecal-F1/2017-09-14_Bloque-0_Acaros.png?timestamp=1505421272");
            listImagesURL.add("https://images.scarab-solutions.com/Florecal-S.A./Florecal-F1/2017-09-14_Bloque-09_Acaros.png?timestamp=1505421301");
            listImagesURL.add("https://images.scarab-solutions.com/Florecal-S.A./Florecal-F1/2017-09-14_Bloque-30_Acaros.png?timestamp=1505421339");
        } else {
            //let's look another example: 2nd image are the widest, 3rd the thinnest
            tableWidths = new float[]{568, 863, 403};
            listImagesURL.add("https://images.scarab-solutions.com/Florecal-S.A./Florecal-F2/2017-09-14_Bloque-13B_Acaros.png?timestamp=1505421413");
            listImagesURL.add("https://images.scarab-solutions.com/Florecal-S.A./Florecal-F2/2017-09-14_Bloque-18A1_Acaros.png?timestamp=1505420968");
            listImagesURL.add("https://images.scarab-solutions.com/Florecal-S.A./Florecal-F2/2017-09-14_Bloque-18A2_Acaros.png?timestamp=1505420973");
        }

        // http://developers.itextpdf.com/examples/tables/table-and-cell-events-draw-borders
        // http://developers.itextpdf.com/examples/tables-itext5/adding-images-table
        PdfPTable table = new PdfPTable(tableWidths);
        table.setWidthPercentage(100);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);

        PdfPCell cell;
        int rows = 5;
        for (int i = 0; i < rows; i++) {
            for (String imageURL : listImagesURL) {
                Image img = Image.getInstance(imageURL);

                cell = new PdfPCell(img, false);
                img.setScaleToFitHeight(true);
                img.setScaleToFitLineWhenOverflow(true);

                table.addCell(cell);
            }
        }
        document.add(table);

        //ending
        document.close();

        ////////////////////////////// DOCUMENT POST-PROCESSING (add current page/total pages at end) //////////////////
        // http://developers.itextpdf.com/examples/columntext-examples-itext5/adding-page-numbers-existing-pdf
        PdfReader reader = new PdfReader(virtualFileDestination.toByteArray());
        int n = reader.getNumberOfPages();
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(destinationFileName));
        PdfContentByte pageContent;
        for (int i = 0; i < n; ) {
            pageContent = stamper.getOverContent(++i);
            ColumnText.showTextAligned(pageContent, Element.ALIGN_BOTTOM, new Phrase(String.format("%s / %s", i, n)), (559 - 26), 22, 0);
        }
        stamper.close();
        reader.close();

        return new ResponseEntity<>("hi", HttpStatus.OK);
    }
}

class HeaderFooter extends PdfPageEventHelper {
    //header and footer
    // http://developers.itextpdf.com/question/how-add-html-headers-and-footers-page
    // http://developers.itextpdf.com/examples/page-events/page-events-headers-and-footers#1331-htmlheaderfooter.java
    private Image scarabLogo = Image.getInstance(new URL("https://testing.scarab-solutions.com/src/app/img/scarabLogo.png"));
    private String farm = "";
    private String dateRange = "";

    HeaderFooter(String farm, String dateRange) throws IOException, BadElementException {
        this.farm = farm;
        this.dateRange = dateRange;
    }

    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        try {
            ColumnText ct = new ColumnText(writer.getDirectContent());
            ////////////////////////////// header //////////////////////////////////////////////////////////////////////
            ct.setSimpleColumn(new Rectangle(36, 832, 559, 810));
            PdfPTable tableHeader = new PdfPTable(2);
            tableHeader.setWidthPercentage(100);
            tableHeader.setWidths(new int[]{1, 9});
            tableHeader.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            //farm name / logo
            PdfPCell cell_farmLogo = new PdfPCell(new Paragraph(farm));
            cell_farmLogo.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_farmLogo.setBorder(Rectangle.NO_BORDER);
            cell_farmLogo.setPadding(0);
            tableHeader.addCell(cell_farmLogo);
            //date range
            PdfPCell cell_dateRange = new PdfPCell(new Paragraph(dateRange));
            cell_dateRange.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell_dateRange.setBorder(Rectangle.NO_BORDER);
            cell_dateRange.setPadding(0);
            tableHeader.addCell(cell_dateRange);
            //add to ColumnText
            ct.addElement(tableHeader);
            ct.go();

            ////////////////////////////// footer //////////////////////////////////////////////////////////////////////
            ct.setSimpleColumn(new Rectangle(36, 10, 559, 32));
            PdfPTable tableFooter = new PdfPTable(2);
            tableFooter.setWidthPercentage(100);
            tableFooter.setWidths(new int[]{1, 9});
            tableFooter.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            //scarab logo
            PdfPCell cell_scarabLogo = new PdfPCell(scarabLogo, true);
            cell_scarabLogo.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_scarabLogo.setBorder(Rectangle.NO_BORDER);
            cell_scarabLogo.setPadding(0);
            tableFooter.addCell(cell_scarabLogo);
            //the other cell is empty (in post-processing page info is added)
            tableFooter.addCell(new Paragraph(""));
            //add to ColumnText
            ct.addElement(tableFooter);
            ct.go();
        } catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }
}
