package com.sagg629.itext.services;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Create many tags for products or whatever, in a page.
 * Created by Sergio A Guzman <sagg629@gmail.com> on 25/10/17 04:32 PM.
 */
@RestController
@RequestMapping("services/r-pdf")
public class ProductsTagsPDF {
    private static final Font FONT_NORMAL = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL);
    private static final Font FONT_NORMAL_BOLD = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
    private static final Font FONT_MEDIUM = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);
    private static final Font FONT_BIG_BOLD = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);

    /**
     * Generate tags in PDF.
     *
     * @param docParams data to build the tags (a JSON sample is located at resources/productTagsSample.json)
     * @return a confirmation message
     * @throws IOException
     * @throws DocumentException
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity generatePDF(@RequestBody Map<String, Object> docParams) throws IOException, DocumentException {
        float tagWidthCM;
        float tagHeightCM;
        if (docParams.get("width").getClass().equals(Integer.class)) {
            tagWidthCM = (Integer) docParams.get("width");
        } else {
            tagWidthCM = ((Double) docParams.get("width")).floatValue();
        }
        if (docParams.get("height").getClass().equals(Integer.class)) {
            tagHeightCM = (Integer) docParams.get("height");
        } else {
            tagHeightCM = ((Double) docParams.get("height")).floatValue();
        }
        List<Map<String, Object>> tagList = (List<Map<String, Object>>) docParams.get("tags");
        int tags2Print = tagList.size();

        String destinationFileName = "productTags.pdf";
        ByteArrayOutputStream documentInMemory = new ByteArrayOutputStream();

        //document definition
        Rectangle pageSize = PageSize.LETTER;
        float[] pageMargins = {5, 5, 5, 15}; //{left, right, top, bottom}
        Document document = new Document(pageSize, pageMargins[0], pageMargins[1], pageMargins[2], pageMargins[3]);
        PdfWriter pdfWriter = PdfWriter.getInstance(document, documentInMemory);
        document.open();
        Float availablePageWidth = (pageSize.getWidth() - (pageMargins[0] + pageMargins[1]));
        Float availablePageHeight = ((Float.parseFloat(String.valueOf(pageSize.getHeight()))) - (Float.parseFloat(String.valueOf(pageMargins[2] + pageMargins[3]))));

        //metadata (http://developers.itextpdf.com/examples/miscellaneous-itext5/custom-metadata-entry)
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        document.addTitle("Documento PDF");
        document.addAuthor("Scarab Solutions");
        document.addCreator("Scarab Solutions");
        document.addSubject(String.format("PDF report of %s, retrieved at %s", "finca", dateFormat.format(new Date())));
        document.addKeywords(String.format("pdf_report, scarab, %s", "finca"));
        document.addCreationDate();

        int printedTags = 0; //conteo de etiquetas impresas
        long rows = Long.MAX_VALUE; //print a maximum of 9223372036854775807 rows [santo santo santo, santo el machetico]

        //dimensions to apply to rectangle (cm --> mm --> points)
        float w = Utilities.millimetersToPoints(cm2mm(tagWidthCM));
        float h = Utilities.millimetersToPoints(cm2mm(tagHeightCM));

        //determine table's column count based on tag's width
        int columnCount = Math.round(availablePageWidth / w);
        float[] tableWidths = new float[columnCount];
        for (int i = 0; i < columnCount; i++) {
            tableWidths[i] = w;
        }

        //set table parameters
        PdfPTable table = new PdfPTable(tableWidths.length);
        table.setWidths(tableWidths);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.setLockedWidth(false);
        table.setWidthPercentage(getTableWidthPercentage(tableWidths, availablePageWidth));
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        for (int rowIndex = 0; rowIndex < rows; rowIndex++) {
            if (printedTags < tags2Print) {
                for (int colIndex = 0; colIndex < tableWidths.length; colIndex++) {
                    if (printedTags < tags2Print) {
                        PdfPTable productTable = getProductTable(tagList.get(printedTags));

                        //mini-table --> cell
                        PdfPCell cell = new PdfPCell(productTable);
                        cell.setFixedHeight(h);
                        cell.setBorder(Rectangle.BOX);
                        cell.setPaddingLeft(10);
                        cell.setPaddingRight(10);
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);
                        printedTags++;
                    } else {
                        break;
                    }
                }
                table.completeRow();
            } else {
                break;
            }
        }

        document.add(table);

        //ending
        document.close();

        generateFile(documentInMemory, destinationFileName);
        return new ResponseEntity<>("pdf OK", HttpStatus.OK);
    }

    /**
     * Convert centimetre --> millimetre
     *
     * @param centimetre value to convert
     * @return converted value
     */
    private float cm2mm(float centimetre) {
        return centimetre * 10;
    }

    /**
     * Calculate table's width percentage based on table's widths and document's available page width
     *
     * @param tableWidths        table's widths (new float[]{50, 700, 50})
     * @param availablePageWidth available space in width to print (734)
     * @return width percentage, 100 if table's width exceeds page limits
     */
    private float getTableWidthPercentage(float[] tableWidths, float availablePageWidth) {
        float tableTotalWidth = 0; //SUM of all widths
        for (float tableWidth : tableWidths) {
            tableTotalWidth += tableWidth;
        }
        //calc percentage of tableTotalWidth against availablePageWidth
        //... for example: if page-width is 842 and table-width is 385, the table occupies 45.72% of paper (in width)
        //... this is the new width percentage for each mini-table and for the MAIN table
        float tableWidthPercentage;
        if (tableTotalWidth > availablePageWidth) {
            //SUM of all widths exceeds page-width limit, table width 100%
            tableWidthPercentage = 100;
        } else {
            tableWidthPercentage = Math.round((tableTotalWidth / availablePageWidth) * 100);
        }
        return tableWidthPercentage;
    }

    /**
     * Get table to fill in each tag
     *
     * @param productDetails an object with product details
     * @return a PdfPTable table
     */
    private PdfPTable getProductTable(Map<String, Object> productDetails) {
        //main table
        PdfPTable productTable = new PdfPTable(1);
        productTable.setWidthPercentage(100);

        PdfPCell cell;
        //farm name
        cell = new PdfPCell(new Paragraph(String.valueOf(productDetails.get("farm")), FONT_NORMAL_BOLD));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_TOP);
        cell.setFixedHeight(16);
        cell.setPaddingTop(-1);
        productTable.addCell(cell);
        productTable.completeRow();
        //product name
        cell = new PdfPCell(new Paragraph(String.valueOf(productDetails.get("product_name")), FONT_BIG_BOLD));
        cell.setBorderWidth(2);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setFixedHeight(40);
        cell.setPaddingBottom(6);
        productTable.addCell(cell);
        productTable.completeRow();
        //spray programme date
        cell = new PdfPCell(new Paragraph(String.valueOf(productDetails.get("spray_date")), FONT_MEDIUM));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setFixedHeight(14); //12
        cell.setPaddingBottom(-2);
        productTable.addCell(cell);
        productTable.completeRow();

        //product details table
        List<Map<String, Object>> productExtendedDetails = (List<Map<String, Object>>) productDetails.get("details");
        PdfPTable productDetailsTable = new PdfPTable(1);
        productDetailsTable.setHorizontalAlignment(Element.ALIGN_CENTER);
        productDetailsTable.setLockedWidth(false);
        productDetailsTable.setWidthPercentage(100);
        productDetailsTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        for (Map<String, Object> detail : productExtendedDetails) {
            cell = new PdfPCell(new Paragraph(String.format("%s: %s", String.valueOf(detail.get("key")), String.valueOf(detail.get("value"))), FONT_NORMAL));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setFixedHeight(16);
            productDetailsTable.addCell(cell);
            productDetailsTable.completeRow();
        }
        //product details --> main table
        cell = new PdfPCell(productDetailsTable);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setPaddingTop(2);
        productTable.addCell(cell);
        productTable.completeRow();

        return productTable;
    }

    private static void generateFile(ByteArrayOutputStream documentInMemory, String fileName) {
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            fos.write(documentInMemory.toByteArray());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
