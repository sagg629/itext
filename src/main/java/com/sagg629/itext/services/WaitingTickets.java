package com.sagg629.itext.services;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

@RestController
@RequestMapping("services/waiting-tickets")
public class WaitingTickets {
    private final String fileName = "waitingTickets.pdf";
    private static final Font FONT_SERIAL = new Font(Font.FontFamily.HELVETICA, 30, Font.BOLD);
    private static final Font FONT_CONTENT_1 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
    private static final Font FONT_CONTENT_2 = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);

    /**
     * Service to print waiting tickets (aka "tiquetes de turno" in spanish)
     * from a JSON input, whose sample is located at resources/waitingTicketsSample.json.
     * It's largely inspired on "product tags PDF", the same used in Scarab Precision's product tags.
     *
     * @param docParams data to build the tickets
     * @return a confirmation message
     * @throws IOException
     * @throws DocumentException
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity generateWaitingTickets(@RequestBody Map<String, Object> docParams) throws IOException, DocumentException {
        float tagWidthCM;
        float tagHeightCM;
        if (docParams.get("width").getClass().equals(Integer.class)) {
            tagWidthCM = (Integer) docParams.get("width");
        } else {
            tagWidthCM = ((Double) docParams.get("width")).floatValue();
        }
        if (docParams.get("height").getClass().equals(Integer.class)) {
            tagHeightCM = (Integer) docParams.get("height");
        } else {
            tagHeightCM = ((Double) docParams.get("height")).floatValue();
        }

        List<Map<String, Object>> allTickets = (List<Map<String, Object>>) docParams.get("tickets");
        int tags2Print = allTickets.size();

        ByteArrayOutputStream documentInMemory = new ByteArrayOutputStream();

        //document definition
        Rectangle pageSize = PageSize.LETTER;
        float[] pageMargins = {5, 5, 5, 15}; //{left, right, top, bottom}
        Document document = new Document(pageSize, pageMargins[0], pageMargins[1], pageMargins[2], pageMargins[3]);
        PdfWriter pdfWriter = PdfWriter.getInstance(document, documentInMemory);
        document.open();
        Float availablePageWidth = (pageSize.getWidth() - (pageMargins[0] + pageMargins[1]));
        Float availablePageHeight = ((Float.parseFloat(String.valueOf(pageSize.getHeight()))) - (Float.parseFloat(String.valueOf(pageMargins[2] + pageMargins[3]))));

        //metadata (http://developers.itextpdf.com/examples/miscellaneous-itext5/custom-metadata-entry)
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        document.addTitle("Documento PDF");
        document.addAuthor("Sergio A. Guzmán <sagg629@gmail.com>");
        document.addCreator("Sergio A. Guzmán <sagg629@gmail.com>");
        document.addSubject(String.format("PDF report of %s, retrieved at %s", "waiting tickets", dateFormat.format(new Date())));
        document.addKeywords(String.format("pdf_report, scarab, %s", "waiting tickets"));
        document.addCreationDate();

        int printedTags = 0; //conteo de etiquetas impresas
        long rows = Long.MAX_VALUE; //print a maximum of 9223372036854775807 rows [santo santo santo, santo el machetico]

        //dimensions to apply to rectangle (cm --> mm --> points)
        float w = Utilities.millimetersToPoints(cm2mm(tagWidthCM));
        float h = Utilities.millimetersToPoints(cm2mm(tagHeightCM));

        //determine table's column count based on tag's width
        int columnCount = Math.round(availablePageWidth / w);
        float[] tableWidths = new float[columnCount];
        for (int i = 0; i < columnCount; i++) {
            tableWidths[i] = w;
        }

        //set table parameters
        PdfPTable table = new PdfPTable(tableWidths.length);
        table.setWidths(tableWidths);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.setLockedWidth(false);
        table.setWidthPercentage(getTableWidthPercentage(tableWidths, availablePageWidth));
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        for (int rowIndex = 0; rowIndex < rows; rowIndex++) {
            if (printedTags < tags2Print) {
                for (int colIndex = 0; colIndex < tableWidths.length; colIndex++) {
                    if (printedTags < tags2Print) {
                        PdfPTable productTable = getProductTable(allTickets.get(printedTags));

                        //mini-table --> cell
                        PdfPCell cell = new PdfPCell(productTable);
                        cell.setFixedHeight(h);
                        cell.setBorder(Rectangle.BOX);
                        cell.setPaddingLeft(10);
                        cell.setPaddingRight(10);
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);
                        printedTags++;
                    } else {
                        break;
                    }
                }
                table.completeRow();
            } else {
                break;
            }
        }

        document.add(table);

        //ending
        document.close();

        generateFile(documentInMemory, fileName);
        return new ResponseEntity<>("pdf OK", HttpStatus.OK);
    }

    /**
     * PRODUCT DETAILS seria cada objeto compuesto por ....
     * {
     * "serial": 139,
     * "time": "11:00"
     * }
     *
     * @param productDetails each object with ticket details
     * @return a PdfPTable table
     */
    private PdfPTable getProductTable(Map<String, Object> productDetails) {
        //main table
        PdfPTable productTable = new PdfPTable(1);
        productTable.setWidthPercentage(100);

        PdfPCell cell;

        // SERIAL
        cell = new PdfPCell(new Paragraph(String.valueOf(productDetails.get("serial")), FONT_SERIAL));
        cell.setBorderWidth(1);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setFixedHeight(40);
        cell.setPaddingTop(-1);
        //cell.setPaddingBottom(6);
        productTable.addCell(cell);
        productTable.completeRow();

        // DATE
        cell = new PdfPCell(new Paragraph("sábado, 04 julio 2020", FONT_CONTENT_1));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setFixedHeight(30);
        productTable.addCell(cell);
        productTable.completeRow();

        // TIME
        cell = new PdfPCell(new Paragraph(String.valueOf(productDetails.get("time")), FONT_CONTENT_2));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setFixedHeight(30);
        productTable.addCell(cell);
        productTable.completeRow();

        System.out.println(productDetails);
        return productTable;
    }

    /**
     * Convert centimetre --> millimetre
     *
     * @param centimetre value to convert
     * @return converted value
     */
    private float cm2mm(float centimetre) {
        return centimetre * 10;
    }

    /**
     * Calculate table's width percentage based on table's widths and document's available page width
     *
     * @param tableWidths        table's widths (new float[]{50, 700, 50})
     * @param availablePageWidth available space in width to print (734)
     * @return width percentage, 100 if table's width exceeds page limits
     */
    private float getTableWidthPercentage(float[] tableWidths, float availablePageWidth) {
        float tableTotalWidth = 0; //SUM of all widths
        for (float tableWidth : tableWidths) {
            tableTotalWidth += tableWidth;
        }
        //calc percentage of tableTotalWidth against availablePageWidth
        //... for example: if page-width is 842 and table-width is 385, the table occupies 45.72% of paper (in width)
        //... this is the new width percentage for each mini-table and for the MAIN table
        float tableWidthPercentage;
        if (tableTotalWidth > availablePageWidth) {
            //SUM of all widths exceeds page-width limit, table width 100%
            tableWidthPercentage = 100;
        } else {
            tableWidthPercentage = Math.round((tableTotalWidth / availablePageWidth) * 100);
        }
        return tableWidthPercentage;
    }

    private static void generateFile(ByteArrayOutputStream documentInMemory, String fileName) {
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            fos.write(documentInMemory.toByteArray());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
