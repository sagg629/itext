/*
 * Copyright (C) 2017 Sergio A Guzman
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sagg629.itext.services.itext7;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.sagg629.itext.utils.LogConsole;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;

/**
 * Created by Sergio A Guzman <sagg629@gmail.com> on 19/09/17 10:22 AM.
 */
@RestController
@RequestMapping("services/pdf-i7")
public class HTML2PDF {
    private final String baseUri = "src/main/resources/templates_html2pdf";

    /**
     * "Hello World" example
     * https://developers.itextpdf.com/content/itext-7-converting-html-pdf-pdfhtml/chapter-1-hello-html-pdf
     *
     * @return ResponseEntity
     * @throws IOException IOException
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity html2PDF() throws IOException {
        final String logPrefix = "[Precision Maps]";
        String src = String.format("%s/precision_maps.html", baseUri);
        String destinationFileName = "lk_i7.pdf";
        LogConsole.printLog(logPrefix, "Start", HTML2PDF.class);

        InputStream htmlSrc = new FileInputStream(src);
        File pdfDest = new File(destinationFileName);
        ConverterProperties converterProperties = new ConverterProperties();
        HtmlConverter.convertToPdf(htmlSrc, new FileOutputStream(pdfDest), converterProperties);

        /*
          NOTAS PERSONALES Y PENSAMIENTOS

          si el SVG posee un tamaño fijo y no dinamico como puede ser una imagen,
          tocaria crear la estructura de la tabla primero
          y definir anchoXalto de cada SVG dependiendo de
          1. cuantas columnas y filas tendra el documento
          2. tamaño de papel (a4 / carta)
          3. orientacion (portrait / landscape)
          esto es para un colorante HTML-->PDF
         */

        LogConsole.printLog(logPrefix, "End - PDF Success", HTML2PDF.class);
        return new ResponseEntity<>("pdf file generated successfully", HttpStatus.OK);
    }
}
