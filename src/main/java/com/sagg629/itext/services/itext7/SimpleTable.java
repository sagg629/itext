package com.sagg629.itext.services.itext7;

import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.xobject.PdfFormXObject;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.itextpdf.layout.property.VerticalAlignment;
import com.itextpdf.svg.converter.SvgConverter;
import com.sagg629.itext.utils.LogConsole;
import com.sagg629.itext.utils.RemoteFiles;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("services/table-i7")
public class SimpleTable {
    final String logPrefix = "[Simple table]";
    final String destinationFileName = "tablita_i7.pdf";
    final String baseURI4LocalFiles = "src/main/resources/SVGs_simpleTable";
    final String baseURLRemoteSVGFiles = "https://reports.scarab-solutions.com/svg/";

    /**
     * Simple table
     *
     * @return a HTTP status with an optional message
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity simpleTable() {
        LogConsole.printLog(logPrefix, "Start", SimpleTable.class);
        try {
            // createTable();
            createTableWithSVG(false);

            return new ResponseEntity<>("PDF ok", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            String message = e.getMessage();
            LogConsole.printLogError(logPrefix, message, SimpleTable.class);
            return new ResponseEntity<>(message, HttpStatus.UNPROCESSABLE_ENTITY);
        } finally {
            LogConsole.printLog(logPrefix, "End", SimpleTable.class);
        }
    }

    /**
     * Create a table with images
     * Simple table: https://kb.itextpdf.com/home/it7kb/examples/101-a-very-simple-table
     * Table with images: https://kb.itextpdf.com/home/it7kb/examples/adding-images-to-a-table#Addingimagestoatable-imagesnexttoeachother
     *
     * @throws FileNotFoundException "File Not Found" exception
     */
    private void createTable() throws IOException {
        final String img1 = "https://images.scarab-solutions.com/Florecal-S.A./Florecal-F2b/2020-11-03_Bloque-18A1_Botritis-de-tallo.png?timestamp=1604432973";
        final String img2 = "https://images.scarab-solutions.com/Florecal-S.A./Florecal-F2a/2020-11-11_Bloque-19A_Acaros.png?timestamp=1605124103";

        PdfDocument pdfDocument = new PdfDocument(new PdfWriter(destinationFileName));
        Document document = new Document(pdfDocument);

        UnitValue[] columnWidths = UnitValue.createPercentArray(2);
        Table table = new Table(columnWidths).useAllAvailableWidth();

        table.addCell(createImageCell(img1));
        table.addCell(createImageCell(img2));

        document.add(table);
        document.close();
    }

    /**
     * Create a table with SVG files
     * SVG to PDF: https://kb.itextpdf.com/home/it7kb/examples/support-for-a-symbol-element-in-svg
     * PDF into another (superimpose): https://kb.itextpdf.com/home/it7kb/examples/superimposing-content-from-one-pdf-into-another-pdf
     *
     * @param getFromRemoteLocation decide to get SVG files from a remote location or just get the local files
     * @throws IOException ioe
     */
    private void createTableWithSVG(boolean getFromRemoteLocation) throws IOException {
        // create a simple table
        PdfDocument pdfDocument = new PdfDocument(new PdfWriter(destinationFileName));
        Document document = new Document(pdfDocument);
        UnitValue[] columnWidths = UnitValue.createPercentArray(2);
        Table table = new Table(columnWidths).useAllAvailableWidth();

        // Precision houses descriptors
        String baseURL_hermosa = String.format("%s/5", baseURLRemoteSVGFiles);
        String baseURL_florecal = String.format("%s/510", baseURLRemoteSVGFiles);
        String baseURL_florecal2 = String.format("%s/16", baseURLRemoteSVGFiles);
        String baseURL_countryFresh = String.format("%s/404", baseURLRemoteSVGFiles);
        String standard1 = String.format("%s/2020-11-16_3710_base.svg", (getFromRemoteLocation ? baseURL_hermosa : baseURI4LocalFiles)); // (hermosa)
        String standard2 = String.format("%s/2020-11-10_3711_base.svg", (getFromRemoteLocation ? baseURL_hermosa : baseURI4LocalFiles)); // (hermosa)
        String smallHeight1 = String.format("%s/2020-11-18_296_base.svg", (getFromRemoteLocation ? baseURL_florecal : baseURI4LocalFiles)); // 18A1 (florecal)
        String smallHeight2 = String.format("%s/2020-11-18_297_base.svg", (getFromRemoteLocation ? baseURL_florecal2 : baseURI4LocalFiles)); // 19A (florecal)
        String tooLarge = String.format("%s/2019-08-08_7362_base.svg", (getFromRemoteLocation ? baseURL_countryFresh : baseURI4LocalFiles)); // 7 (country fresh)

        // convert local SVG files into separate PdfFormXObject to be used and manipulated in the main PDF
        List<String> svgs2Convert = new ArrayList<>(Arrays.asList(standard1, standard2, smallHeight1, smallHeight2, tooLarge));
        for (String eachSVGFilePath : svgs2Convert) {
            try {
                String eachSVGFileNameWithoutPath = new File(eachSVGFilePath).getName();
                InputStream svg = (getFromRemoteLocation ? new RemoteFiles().getInputStream(eachSVGFilePath) : new FileInputStream(eachSVGFilePath));
                PdfFormXObject pdfFormXObject = SvgConverter.convertToXObject(svg, pdfDocument);

                // iText PdfFormXObject --> table
                table.addCell(createImageCell(pdfFormXObject, eachSVGFileNameWithoutPath));
            } catch (Exception e) {
                LogConsole.printLogError(logPrefix, String.format("\t Error processing SVG file (reason: %s)", e.getMessage()), SimpleTable.class);
                // table.addCell("").setBorder(Border.NO_BORDER);
            }
        }

        document.add(table);
        document.close();
    }

    /**
     * Create image cell (cell with a iText image)
     *
     * @param pdfObject a PdfFormXObject element (special object from a PDF document)
     * @return a Cell to be appended to a table
     */
    private Cell createImageCell(PdfFormXObject pdfObject, String text) {
        Cell cell = new Cell();
        Image img = new Image(pdfObject);
        img.setAutoScaleHeight(true);
        img.setMaxWidth(img.getImageWidth());
        img.setMaxHeight(img.getImageHeight());

        if (text != null) {
            cell.add(createTextCell(text));
        }
        cell.add(img);
        cell.setBorder(Border.NO_BORDER);
        return cell;
    }

    /**
     * Create image cell (cell with an image)
     *
     * @param imagePath path or URL of an image
     * @return a Cell to be appended to a table
     * @throws MalformedURLException exception when the image URL is incorrect or malformed
     */
    private Cell createImageCell(String imagePath) throws MalformedURLException {
        Image img = new Image(ImageDataFactory.create(imagePath));
        img.setAutoScale(true);
        img.setWidth(UnitValue.createPercentValue(100));
        return new Cell().add(img);
    }

    /**
     * Create text cell
     *
     * @param text text to be set into the Cell
     * @return a Cell or a null in case of empty or null text
     */
    private Cell createTextCell(String text) {
        if (text != null && !text.isEmpty()) {
            Cell cell = new Cell();
            Paragraph p = new Paragraph(text);
            p.setTextAlignment(TextAlignment.LEFT);
            p.setFontSize(8);
            cell.add(p).setVerticalAlignment(VerticalAlignment.MIDDLE);
            cell.setBorder(Border.NO_BORDER);
            return cell;
        }
        return null;
    }
}
