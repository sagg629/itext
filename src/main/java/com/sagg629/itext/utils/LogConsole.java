package com.sagg629.itext.utils;

import org.apache.log4j.Logger;

/**
 * Small class to print messages to System out
 */
public class LogConsole {
    /**
     * Print something to the system log (System Out)
     *
     * @param logPrefix log prefix, for example "[XLSX - Spray Programme]"
     * @param message   message to show
     * @param theClass  class to identify the origin of the message
     */
    public static void printLog(String logPrefix, String message, Class theClass) {
        Logger log = Logger.getLogger((theClass != null) ? theClass : LogConsole.class);
        log.info(String.format("%s %s", logPrefix, message));
    }

    /**
     * Also print a message to log ... but as an ERROR
     *
     * @param logPrefix log prefix, for example "[XLSX - Spray Programme]"
     * @param message   message to show
     * @param theClass  class to identify the origin of the message
     */
    public static void printLogError(String logPrefix, String message, Class theClass) {
        Logger log = Logger.getLogger((theClass != null) ? theClass : LogConsole.class);
        log.error(String.format("%s %s", logPrefix, message));
    }
}
