package com.sagg629.itext.utils;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Little util class to manage files in a remote location
 */
public class RemoteFiles {
    final String logPrefix = "[Remote File Utils]";

    /**
     * Get input stream
     * https://stackoverflow.com/a/28215776/3135458
     *
     * @param rawURL URL of the remote file
     * @return an InputStream
     */
    public InputStream getInputStream(String rawURL) {
        try {
            if (rawURL != null && !rawURL.isEmpty()) {
                LogConsole.printLog(logPrefix, String.format("Retrieving file from %s ...", rawURL), RemoteFiles.class);
                final URL url = new URL(rawURL);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                return urlConnection.getInputStream();
            } else {
                throw new Exception("URL not valid");
            }
        } catch (Exception e) {
            LogConsole.printLogError(logPrefix, String.format("Error retrieving file (reason: %s)", e.getMessage()), RemoteFiles.class);
            return null;
        }
    }
}
